package grafo;

import java.util.ArrayList;

//Representaci�n con matriz de adyacencia

public class grafo {
	
	private boolean [][] _adyacencia;
	private int _vertice;
	
	public grafo(int vertices)
	{
		_adyacencia = new boolean [vertices][vertices];
		_vertice=vertices;
	
	}
	
	public void agregarArista (int verticeI,int verticeJ)
	{
		//bucle
		//que ya no este
		//mayores que 0 y menores que vertice
		
		if(verticeI==verticeJ)
		{
			throw new IllegalArgumentException("Se intento agregar una arista "); 
		}
		
		if (verticeI < 0 || verticeI >= _vertice)
		{
			throw new IllegalArgumentException("Se intento agregar una arista donde el verticeI encuentra fuera del rango");
		}
		
		if (verticeJ < 0 || verticeJ >= _vertice)
		{
			throw new IllegalArgumentException("Se intento agregar una arista donde el verticeI se encuentra fuera del rango");
		}
		
		_adyacencia[verticeI][verticeJ]= true;
		_adyacencia[verticeJ][verticeI]= true;
		
		}
	
	public void eliminarArista(int verticeI,int verticeJ)
	{
		if(verticeI==verticeJ)
		{
			throw new IllegalArgumentException("Se intento agregar una arista "); 
		}
		
		if (verticeI < 0 || verticeI >= _vertice)
		{
			throw new IllegalArgumentException("Se intento agregar una arista donde el verticeI encuentra fuera del rango");
		}
		
		if (verticeJ < 0 || verticeJ >= _vertice)
		{
			throw new IllegalArgumentException("Se intento agregar una arista donde el verticeI se encuentra fuera del rango");
		}
		
		_adyacencia[verticeI][verticeJ]= false;
		_adyacencia[verticeJ][verticeI]= false;
		
	}
	
	public boolean existeArista(int verticeI, int verticeJ)
	{
		verificarArista(verticeI, verticeJ,"consultar");
		
		return _adyacencia[verticeI][verticeJ];
	}

	private void verificarArista(int verticeI, int verticeJ, String tipo) 
	{
		if(verticeI==verticeJ)
		{
			throw new IllegalArgumentException("Se intento" + tipo + " una arista que es un bucle "); 
		}
		
		verificarVertice(verticeI, tipo);
		
		verificarVertice(verticeJ, tipo);
	}

	private void verificarVertice(int verticeI, String tipo) {
		if (verticeI < 0 || verticeI >= _vertice)
		{
			throw new IllegalArgumentException("Se intento " + tipo + " un vertice que se encuentra fuera del rango");
		}
	}
		
	public ArrayList<Integer> vecinos (int vertice)
	{
		verificarVertice(vertice, "un vertice");
		ArrayList<Integer> ret = new ArrayList<Integer>();
		
		for(int col = 0; col < _vertice;col++)
		{
			if(vertice != col && existeArista(vertice,col))
			{
				ret.add(col);
			}
		}
		
		return ret;
	}
	
	public int grado (int vertice)
	{
		return vecinos(vertice).size();
	}
	
}

