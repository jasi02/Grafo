package grafo;

public class Main {

	public static void main(String[] args) {

		grafo g = new grafo (5);
		
		g.agregarArista(0, 1);
		g.agregarArista(1, 2);
		g.agregarArista(1, 3);
		g.agregarArista(0, 3);
		g.agregarArista(3, 4);
		
		System.out.println("Existe arista 1,0 " + g.existeArista(1, 0));
		System.out.println("Existe arista 0,1 " + g.existeArista(1, 0));
		System.out.println("Existe arista 3,0 " + g.existeArista(3, 0));
		System.out.println("Existe arista 3,4 " + g.existeArista(3, 4));
		
		
		
	}

}
